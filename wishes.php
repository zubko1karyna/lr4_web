<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Wish list</title>
  <link href="css/style.css" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
       <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

      <!-- Bootstrap core CSS -->
  <link href="/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

      <!-- Favicons -->
  <link rel="apple-touch-icon" href="/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
  <link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
  <link rel="icon" href="/docs/4.5/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
  <link rel="manifest" href="/docs/4.5/assets/img/favicons/manifest.json">
  <link rel="mask-icon" href="/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
  <link rel="icon" href="/docs/4.5/assets/img/favicons/favicon.ico">
  <meta name="msapplication-config" content="/docs/4.5/assets/img/favicons/browserconfig.xml">
  <meta name="theme-color" content="#563d7c">
</head>
<header>
    <?php
    require "blocks/header.php";
    ?>
  </header>
<body>
<h2 class="mb-5 annonced">your wish list</h2>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-3"></div> <!--Пустой блок справа-->
                <div class="col-md-6">
                    <form method = "POST" action = "add.php">
                       <div class="form-group">
                           <label for="exampleInputEmail1">Purchase name</label>
                             <input type="text" name="wish" id="wish" placeholder="Enter your wish item" class="form-control">
                        </div>
                        <button type="submit" name="sendTask" class="btn btn-success">Send</button>
                    </form>
                    <?php
                        require'dbcon.php';
                        /**
                        *query виконуємо запит на отримання усіх данних з таблиці
                        *FETCH_OBJ отримуємо дані у вигляді об'єктів
                        */
                        echo '<ul>';
                        $query = $pdo->query('SELECT * FROM `wishes` ORDER BY `id` DESC');
                        while($row = $query->fetch(PDO::FETCH_OBJ)) {
                        echo '<li><b>'.$row->wish.'</b><a href="/delete.php?id='.$row->id.'"><button>Delete</button></a></li>';
                        }
                        echo '</ul>';
                    ?>
                    </div>
                </div>
            <div class="col-md-3"></div><!--Пустой блок слева-->
        </div>
    </div>
  </body>
</html>

