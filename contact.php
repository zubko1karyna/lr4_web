<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact us</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  </head>
  <body>
  <header>
    <?php require "blocks/header.php"?>
  </header>
<div class="container mt-5"> 
  <h2 class="mb-5 annonced"> contact us </h2>
  <form action="check.php" method="post">
    <input type="email" name="email" placeholder="Enter an email" class="form-control"><br>

    <textarea name="message" class="form-control" placeholder="Enter your message"></textarea><br>
    
    <button type="submit" name="send" class="btn btn-success"> Send </button>
  </form>
</div>
    <?php require "blocks/footer.php"?>
</body>
</html>